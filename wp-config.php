<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sw_dev');

/** MySQL database username */
define('DB_USER', 'wordpress_admin');

/** MySQL database password */
define('DB_PASSWORD', 'CXv7AFsCzKhbFTY6');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Pqg`m5RX@:(gD3C(>^O;.S=QaY7I0OJ;0liRfTybe@1kMkt9U;Us1xh.&&1hd(ev');
define('SECURE_AUTH_KEY',  'D+{*fKif<(?G;&q<W&K4uYzvY~s+hFDPkt_&n}(rukcaM:O98t6 nm`af!L_dA<1');
define('LOGGED_IN_KEY',    '>4&Al1wV{3Iycb~&GErKGRecXIUgFmdBr{`1@.H!GOnW/F)o2PC%mxQplcK-+W<#');
define('NONCE_KEY',        ' z/Z`+_@:4P[Vs!5JCF5TlE7?&JuMnUf|V#Qh]dSilJGBakb}.m2skPGoq`bIT=E');
define('AUTH_SALT',        'cp(/ZBN?bX&^Smn&xyDiF$1aZA!t2d*xQzMdo`yQsR1D5)knaew;,,$*<@~&|3X^');
define('SECURE_AUTH_SALT', '0`}*1Bs5ZjDV[i+gu$X@|NDI5H,]7JRz,Yu4v;W2?FzS6m~mDS2DNVSgIY*@B~s0');
define('LOGGED_IN_SALT',   'I1Ilfk=.KsL|[]/K=).PV{23NvEdbYLC2Ds1&fQ;HGxXe~rlP#IEE)T3h$Zn9kZ ');
define('NONCE_SALT',       '4@mywUiUBMAi_$+v=2W`usyaU%D4*ny.@6IF)o>0$kU(V-22s_vW~KvTx%-ka/}{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
