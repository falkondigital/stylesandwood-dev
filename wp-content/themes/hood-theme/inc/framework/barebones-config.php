<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "hood_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Hood Options', 'hood-theme' ),
        'page_title'           => esc_html__( 'Hood Options', 'hood-theme' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => 'http://www.hoodthemes.com/hood-ico.png',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.
        'templates_path'            => get_template_directory() . '/inc/framework/templates/panel',

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'hood-theme' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'hood-theme' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'hood-theme' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '', 'hood-theme' ), $v );
    } else {
        $args['intro_text'] = esc_html__( '', 'hood-theme' );
    }

    // Add content after the form.
    $args['footer_text'] = esc_html__( '', 'hood-theme' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

  

    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
         Redux::setSection( $opt_name, array(
        'icon' => 'el el-laptop',
        // Set the class for this icon.
        // This field is ignored unless $args['icon_type'] = 'iconfont'
        'icon_class' => 'icon-large',
        'title' => esc_html__('Getting Started', 'hood-theme'),
        'desc' => esc_html__('', 'hood-theme'),
        'fields' => array(
            array(
                'id' => 'font_awesome_info',
                'type' => 'info',
                'desc'     => '<h3 style="text-align: center; border-bottom: none;">Welcome to the Options panel of the '.wp_get_theme()->get( 'Name' ).'!</h3>
                                    <h4 style="text-align: center; font-size: 1.3em;">What does this mean to you?</h4>
                                    <p style="text-align: center;">From here on you will be able to regulate the main options of all the elements of the theme. </p>
                                    <p style="text-align: center;">Theme documentation you will find here: <a href="http://documentation.hoodthemes.com" target="_blank">http://documentation.hoodthemes.com</a></p>',
          ),
        )
    ) );

      Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Settings', 'hood-theme' ),
        'id'         => 'general-setting',
        'icon'   => 'el el-magic',
        'desc'       => esc_html__('', 'hood-theme'),
        'fields'     => array(
             array(
                'id'       => 'general-web-color',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Predefined Color Scheme', 'hood-theme' ),
                'subtitle' => esc_html__( 'Controls the main color scheme throughout the theme. Select a scheme and all color options will change to the defined selection.', 'hood-theme' ),
                'default'  => 'custom-color.php',
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'custom-color.php' => get_template_directory_uri().'/assets/img/color/no-color.jpg',
                    'awesome.css' => get_template_directory_uri().'/assets/img/color/awesome.jpg',
                    'color-backery.css' => get_template_directory_uri().'/assets/img/color/color-backery.jpg',
                    'bleu-de-france.css' => get_template_directory_uri().'/assets/img/color/bleu-de-france.jpg',
                    'rich-electric-blue.css' => get_template_directory_uri().'/assets/img/color/rich-electric-blue.jpg',
                    'chateau-green.css' => get_template_directory_uri().'/assets/img/color/chateau-green.jpg',
                    'niagara.css' => get_template_directory_uri().'/assets/img/color/niagara.jpg',
                    'deep-lilac.css' => get_template_directory_uri().'/assets/img/color/deep-lilac.jpg',
                    'dark-pastel-red.css' => get_template_directory_uri().'/assets/img/color/dark-pastel-red.jpg',
                    'orange.css' => get_template_directory_uri().'/assets/img/color/orange.jpg',
                    'my-sin.css' => get_template_directory_uri().'/assets/img/color/my-sin.jpg',

                ),
               
            ),
            array(
                    'id'       => 'general-custom-web-color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Primary Color', 'hood-theme' ),
                    'subtitle' => esc_html__( 'Controls the main highlight color throughout the theme.', 'hood-theme' ),

                    'default'  => '#3083c9',
                ),
             array(
                'id'       => 'default-pg-bg2',
                'type'     => 'info',
                'desc'    => esc_html__( 'Link Color', 'hood-theme' ),
            ),
             array(
                'id'       => 'a-link',
                'type'     => 'color',
                'output'    => array( 'a' ),
                'title'    => esc_html__( 'General Link Color', 'hood-theme' ),
                'default'  => '#3083c9',
            ),
             array(
                'id'       => 'a-hover',
                'type'     => 'color',
                'output'    => array( 'a:hover' ),
                'title'    => esc_html__( 'General Link Hover', 'hood-theme' ),
                'default'  => '#0074bc',
            ),
             array(
                'id'       => 'a-active',
                'type'     => 'color',
                'output'    => array( 'a:active' ),
                'title'    => esc_html__( 'General Link Active', 'hood-theme' ),
                'default'  => '#008ccf',
            ),
             array(
                'id'       => 'a-visited',
                'type'     => 'color',
                'output'    => array( 'a:visited' ),
                'title'    => esc_html__( 'General Link Visited', 'hood-theme' ),
                'default'  => '#008ccf',
            ),
            )
    ) );    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Header', 'hood-theme' ),
        'id'     => 'basic',
        'desc'   => esc_html__( 'Basic field with no subsections.', 'hood-theme' ),
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Version', 'hood-theme' ),
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
           array(
                'id'       => 'header-version',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Select Header Version', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( '', 'hood-theme' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '1' => get_template_directory_uri().'/assets/img/header_v1.jpg',
                    '2' => get_template_directory_uri().'/assets/img/header_v2.jpg',
                    '3' => get_template_directory_uri().'/assets/img/header_v3.jpg',
                    '4' => get_template_directory_uri().'/assets/img/header_v4.jpg',
                    '5' => get_template_directory_uri().'/assets/img/header_v5.jpg',
                    '6' => get_template_directory_uri().'/assets/img/header_v6.jpg',
                    '7' => get_template_directory_uri().'/assets/img/header_v7.jpg',
                    '8' => get_template_directory_uri().'/assets/img/header_v8.jpg',
                    '9' => get_template_directory_uri().'/assets/img/header_v9.jpg',
                    '10' => get_template_directory_uri().'/assets/img/header_v10.jpg',

                ),
                'default'  => '2'
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Background', 'hood-theme' ),
        'id'         => 'header_back',
        'subsection' => true,
        'fields'     => array(
             array(
                'id'       => 'header_background',
                'type'     => 'background',
                'output'    => array( '.header-body' ),
                'title'    => esc_html__('Header Background Image', 'hood-theme'),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'hood-theme'),
                'desc'     => esc_html__('', 'hood-theme'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),
        ),

    ) );


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Styling', 'hood-theme' ),
        'id'         => 'opt-styling',
        'subsection' => true,
        'fields'     => array(

            array(
                'id'       => 'opt-header-top-line2',
                'type'     => 'border',
                'title'    => esc_html__( 'Header Version 4 & 10 Top Border', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'output'   => array( '.br-top3' ),
                // An array of CSS selectors to apply this font style to
                'desc'     => esc_html__( '', 'hood-theme' ),
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#3083c9',
                    'border-style'  => 'solid',
                    'border-top'    => '3px',
                    'border-right'  => '0px',
                    'border-bottom' => '0px',
                    'border-left'   => '0px'
                )
            ),
            array(
                'id'       => 'top-line2',
                'type'     => 'color',
                'output'    => array( '.header-top-light' ),
                'title'    => esc_html__( 'Header Top Line Color 2', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( '(Header Version 6,9,10)', 'hood-theme' ),
                'default'  => '#f8f8f8',
                'mode'     => 'background',
            ),

            array(
                'id'       => 'opt-header-top-half',
                'type'     => 'color',
                'output'    => array( '.header-top8'),
                'title'    => esc_html__( 'Header Version 8 Top Half Box (Left)', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'default'  => '#f8f8f8',
                'mode'     => 'background'
            ),
            array(
                'id'       => 'opt-header-top-half-triangle',
                'type'     => 'border',
                'title'    => esc_html__( 'Header Version 8 Triangle', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'output'   => array( '.header-top-v8:before' ),
                // An array of CSS selectors to apply this font style to
                'desc'     => esc_html__( '', 'hood-theme' ),
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#f8f8f8',
                    'border-style'  => 'solid',
                    'border-left'   => '20px'
                )
            ),
            

        ),

    ) );



    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Options', 'hood-theme' ),
        'id'         => 'opt-text-header-options',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'search-box',
                'type'     => 'switch',
                'title'    => esc_html__( 'Search Box', 'hood-theme' ),
                'default'  => true,
            ),
             array(
                'id'       => 'sticky_header_1',
                'type'     => 'switch',
                'title'    => esc_html__( 'Sticky Header', 'hood-theme' ),
                'subtitle' => esc_html__( 'Turn on to enable a sticky header.', 'hood-theme' ),
                'desc'     => esc_html__( '', 'hood-theme' ),
                'default'  => true,
            ),
            array(
                'id'       => 'header_cart',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Cart Icon', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( '', 'hood-theme' ),
                'default'  => false,
            ),
            array(
                'id'       => 'text-col-telephone',
                'type'     => 'text',
                'title'    => esc_html__( 'Telephone Text', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( 'Show Header Version 3,6,8,10', 'hood-theme' ),
                'default'  => 'Telephone',
            ),
            array(
                'id'       => 'text-telephone',
                'type'     => 'text',
                'title'    => esc_html__( 'Telephone Number', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( 'Show Header Version 3,6,8,10', 'hood-theme' ),
                'default'  => '+0800 123 4567',
            ),
            array(
                'id'       => 'text-col-mail',
                'type'     => 'text',
                'title'    => esc_html__( 'Email Text', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( 'Show Header Version 3,6,8,10', 'hood-theme' ),
                'default'  => 'E-Mail',
            ),
             array(
                'id'       => 'text-mail',
                'type'     => 'text',
                'title'    => esc_html__( 'Email Adress', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__( 'Show Header Version 3,6,8,10', 'hood-theme' ),
                'default'  => '+0800 123 4567',
            ),
             array(
                'id'       => 'opt-tel-mail-color-light',
                'type'     => 'color',
                'output'    => array( '.c-header-info' ),
                'important' => true,
                'mode'      => 'color',
                'title'    => esc_html__( 'Telephone & Email Text Color Light', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#747474',
            ),
              array(
                'id'       => 'opt-tel-mail-color-dark',
                'type'     => 'color',
                'output'    => array( '.header-top-v8 .c-header-info' ),
                'important' => true,
                'mode'      => 'color',
                'title'    => esc_html__( 'Telephone & Email Text Color Dark', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#FFFFFF',
            ),
        ),

    ) );


     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo & Favicon', 'hood-theme' ),
        'id'         => 'logo-favicon',
        'icon'   => 'el el-bookmark',
        'desc'       => esc_html__('', 'hood-theme'),
        'fields'     => array(
        	array(
                'id'             => 'logo-width',
                'type'           => 'dimensions',
                'output'    => array( '.header .logo a img' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => __( 'Logo (Width)', 'hood-theme' ),
                'height'         => false,
                'default'        => array(
                    'width'  => 125,
                )
            ),
            array(
                'id'             => 'logo-height',
                'type'           => 'dimensions',
                'output'    => array( '.desk .logo' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => __( 'Logo (Height)', 'hood-theme' ),
                'height'         => true,
                'width'          => false,
                'default'        => array(
                    'height'  => 105,
                )
            ),
            array(
                'id'       => 'logo-padding',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('em', 'px'),
                'title'    => esc_html__( 'Logo Padding', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'output'   => array( '.desk .logo' ),
                // An array of CSS selectors to apply this font style to
                'desc'     => esc_html__( '', 'hood-theme' ),
                'default'  => array(
                    'margin-top'     => '5px', 
			        'margin-right'   => '0px', 
			        'margin-bottom'  => '5px', 
			        'margin-left'    => '0px',
                )
            ),
            array(
                'id'       => 'main-logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Logo', 'hood-theme' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'hood-theme'),
                'subtitle' => esc_html__('', 'hood-theme'),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'             => 'mobile-logo-width',
                'type'           => 'dimensions',
                'output'    => array( '.logo-mob img' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => __( 'Mobile Logo (Width)', 'hood-theme' ),
                'height'         => false,
                'default'        => array(
                    'width'  => 100,
                )
            ),
            array(
                'id'       => 'mobile-logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Mobile Logo', 'hood-theme' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'hood-theme'),
                'subtitle' => esc_html__('', 'hood-theme'),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),

            array(
                'id'       => 'favicon',
                'type'     => 'media',
                'title'    => esc_html__( 'Favicon', 'hood-theme' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'hood-theme'),
                'subtitle' => esc_html__('', 'hood-theme'),
                ),

             array(
                'id'       => 'apple-icon',
                'type'     => 'media',
                'title'    => esc_html__( 'Apple iPhone Icon', 'hood-theme' ),

                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'hood-theme'),
                'subtitle' => esc_html__('', 'hood-theme'),
                ),
            )
    ) );


     // Menu
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Menu', 'hood-theme' ),
        'id'         => 'opt-menu-styling',
        'icon'   => 'el el-th-list',
        'fields'     => array(
            array(
                'id'       => 'menu-bg',
                'type'     => 'color',
                'output'    => array( '.version2 .header-menu' ),
                'title'    => esc_html__( 'Menu Background', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'background',
                'default'  => '#FFFFFF',
            ),
             array(
                'id'       => 'opt-menu-link-color',
                'type'     => 'typography',
                'output' => array('.version2 .desk-menu > ul > li > a','.version1 .desk-menu > ul > li > a'),
                'title'    => esc_html__( 'Menu Font', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'google'   => true,
                'line-height' => false,                
                'default'  => array(
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'color'       => '#424141',
                    'google'      => true,
                    'font-size'   => '13px',
                ),
            ),
            array(
                'id'       => 'opt-menu-hover-color',
                'type'     => 'color',
                'output'    => array('.version2 .desk-menu > ul > li > a:hover','.version1 .desk-menu > ul > li > a:hover'),
                'title'    => esc_html__( 'Menu Hover Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'color',
                'default'  => '#424141',
            ),
            array(
                'id'       => 'opt-menu-active-color',
                'type'     => 'color',
                'output'    => array( '.version2 .desk-menu > ul > li > a:active','.version1 .desk-menu > ul > li:hover > a, .version1 .desk-menu > ul > li.one-page-active > a, .version1 .desk-menu > ul > li.current-menu-item > a, .version1 .desk-menu > ul > li.current-menu-parent > a, .version1 .desk-menu > ul > li.current-menu-ancestor > a' ),
                'title'    => esc_html__( 'Menu Active Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'color',
                'default'  => '#424141',
            ),
            array(
                'id'       => 'menu-effect-color',
                'type'     => 'color',
                'output'    => array( '.version1.effect-underline .desk-menu > ul > li > a:after, .version1.effect-overline .desk-menu > ul > li > a:after, .version1.effect-fill .desk-menu > ul > li:hover > a, .version1.effect-fill .desk-menu > ul > li.one-page-active > a, .version1.effect-fill .desk-menu > ul > li.current-menu-item > a, .version1.effect-fill .desk-menu > ul > li.current-menu-parent > a, .version1.effect-fill .desk-menu > ul > li.current-menu-ancestor > a','.version2.effect-underline .desk-menu > ul > li > a:after, .version2.effect-overline .desk-menu > ul > li > a:after, .version2.effect-fill .desk-menu > ul > li:hover > a, .version2.effect-fill .desk-menu > ul > li.one-page-active > a, .version2.effect-fill .desk-menu > ul > li.current-menu-item > a, .version2.effect-fill .desk-menu > ul > li.current-menu-parent > a, .version2.effect-fill .desk-menu > ul > li.current-menu-ancestor > a' ),
                'title'    => esc_html__( 'Menu Hover & Active Effect Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'background',
                'default'  => '#3083c9',
            ),
            array(
                'id'       => 'dropdown-effect',
                'type'     => 'select',
                'title'    => __( 'Submenu Effect', 'hood-theme' ),
                //Must provide key => value pairs for select options
                'options'  => array(
                    'fade' => 'Fade',
                    'slide' => 'Slide',
                    'move' => 'Move',
                    'ghost' => 'Under',
                ),
                'default'  => 'slide'
            ),
            array(
                'id'       => 'mega_menu_drop_typ',
                'type'     => 'typography',
                'output' => array('.version1 .desk-menu .sub-menu li > a','.version2 .desk-menu .sub-menu li > a'),
                'title'    => esc_html__( 'Dropdown & Mega Menu Fonts', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'google'   => true,
                'line-height' => false,                
                'default'  => array(
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'color'       => '#999999',
                    'google'      => true,
                    'font-size'   => '13px',
                ),
            ),
             array(
                'id'       => 'megamenu_bg',
                'type'     => 'color',
                'output'    => array( '.version1 .desk-menu .sub-menu, .version1 .header-top .header-top-menu ul li ul, .version1 .search, .version1 .woo-cart','.version2 .desk-menu .sub-menu, .version2 .header-top .header-top-menu ul li ul, .version2 .search, .version2 .woo-cart' ),
                'title'    => esc_html__( 'Dropdown Menu & Mega Menu Backgorund', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'background',
                'default'  => '#292929',
            ),
             array(
                'id'       => 'dropdown_sub_sub',
                'type'     => 'color',
                'output'    => array( '.version2 .desk-menu .sub-menu .sub-menu','.version1 .desk-menu .sub-menu .sub-menu' ),
                'title'    => esc_html__( 'Dropdown Menu Sub Menu Sub Backgorund', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'background',
                'default'  => '#151515',
            ),   
              array(
                'id'       => 'dropdown_sub_a_hover',
                'type'     => 'color',
                'output'    => array( '.version1 .desk-menu .sub-menu li:hover > a, .version1 .header-top .header-top-menu ul li ul li:hover > a, .version2 .desk-menu .sub-menu li:hover > a' ),
                'title'    => esc_html__( 'Dropdown Menu Link Hover Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'desc'     => esc_html__('', 'hood-theme'),
                'mode'     => 'color',
                'default'  => '#efefef',
            ),  

            

        ),

    ) );



    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer', 'hood-theme' ),
        'id'         => 'opt-text-footer-options',
        'icon'   => 'el el-fork',
        'fields'     => array(
             array(
                'id'       => 'footer-columns',
                'type'     => 'select',
                'title'    => __( 'Footer Columns', 'hood-theme' ),
                //Must provide key => value pairs for select options
                'options'  => array(
                    '1' => '1 Column',
                    '2' => '2 Column',
                    '3' => '3 Column',
                    '4' => '4 Column',
                ),
                'default'  => '4'
            ),
             array(
                'id'       => 'opt-color-bg-footer',
                'type'     => 'color',
                'output'    => array( '.c-layout-footer .c-prepfooter' ),
                'title'    => esc_html__( 'Footer Backgorund Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#262727',
                'mode'     => 'background',
            ),
             array(
                'id'       => 'opt-color-title-footer',
                'type'     => 'color',
                'output'    => array( '.c-layout-footer .c-prepfooter .c-caption' ),
                'title'    => esc_html__( 'Footer Title Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#FFFFFF',
                'mode'     => 'color',
            ),
            array(
                'id'       => 'opt-color-content-footer',
                'type'     => 'color',
                'output'    => array( '.c-layout-footer .c-prepfooter a' ),
                'title'    => esc_html__( 'Footer Content Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#bfbfbf',
                'mode'     => 'color',
            ),
            array(
                'id'       => 'opt-color-bg-bottom-footer',
                'type'     => 'color',
                'output'    => array( '.c-layout-footer .c-postfooter' ),
                'title'    => esc_html__( 'Footer Bottom Backgorund Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#363839',
                'mode'     => 'background',
            ),
            array(
                'id'       => 'opt-color-content-footer-bottom',
                'type'     => 'color',
                'output'    => array( '.c-layout-footer .c-postfooter' ),
                'title'    => esc_html__( 'Footer Bottom Content Color', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '#FFFFFF',
            ),
        ),

    ) );

    
      // -> START Typography
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Styling', 'hood-theme' ),
        'id'     => 'styling-main',
        'desc'   => esc_html__( '', 'hood-theme' ),
        'icon'   => 'el  el-magic',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Search', 'hood-theme' ),
        'id'         => 'opt-menu-search',
        'subsection' => true,
        'fields'     => array(
             array(
                'id'       => 'opt-search-light-button-color',
                'type'     => 'color',
                'output'    => array( '.search-light' ),
                'title'    => esc_html__( 'Light Bg Search Button Color', 'hood-theme' ),
                'subtitle' => esc_html__( 'Header Version: 1,3,5,6,7,8 ', 'hood-theme' ),
                'default'  => '#3083c9',
            ),
            array(
                'id'       => 'opt-search-dark-button-color',
                'type'     => 'color',
                'output'    => array( '.search-dark' ),
                'title'    => esc_html__( 'Dark Bg Search Button Color', 'hood-theme' ),
                'subtitle' => esc_html__( 'Header Version: 2,9,10 ', 'hood-theme' ),
                'default'  => '#FFFFFF',
            ),
        ),

    ) );

      // -> START Typography
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Typography', 'hood-theme' ),
        'id'     => 'typography',
        'desc'   => esc_html__( 'For full documentation on this field, visit: ', 'hood-theme' ) . '<a href="//docs.reduxframework.com/core/fields/typography/" target="_blank">docs.reduxframework.com/core/fields/typography/</a>',
        'icon'   => 'el el-fontsize',
    ) );

     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Font Settings', 'hood-theme' ),
        'id'         => 'opt-text-general-font',
        'subsection' => true,
        'fields'     => array(
           
            array(
                'id'       => 'opt-typography-body',
                'type'     => 'typography',
                'output'    => array( 'body' ),
                'title'    => esc_html__( 'Body Font', 'hood-theme' ),
                'subtitle' => esc_html__( 'Specify the body font properties.', 'hood-theme' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#747474',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'line-height' => '24px',
                    'font-size'   => '14px',
                ),
            ),
            array(
                'id'       => 'opt-typography-post',
                'type'     => 'typography',
                'output'    => array( '.c-layout-breadcrumbs-1 .c-page-title' ),
                'title'    => esc_html__( 'Page & Post Header H1 Title', 'hood-theme' ),
                'subtitle' => esc_html__( 'Specify the page & post header h1 font properties.', 'hood-theme' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#727070',
                    'font-weight'  => '300',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '27px',
                    'line-height' => '35px'
                ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Heading Fonts', 'hood-theme' ),
        'id'         => 'opt-text-heading-font',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'          => 'opt-typography-h1',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H1', 'hood-theme' ),
                'output'    => array( 'h1' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '36px',
                    'line-height' => '35px'
                ),
            ),
             array(
                'id'          => 'opt-typography-h2',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H2', 'hood-theme' ),
                'output'    => array( 'h2' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '30px',
                    'line-height' => '30px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h3',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H3', 'hood-theme' ),
                'output'    => array( 'h3' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '24px',
                    'line-height' => '30px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h4',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H4', 'hood-theme' ),
                'output'    => array( 'h4' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '18px',
                    'line-height' => '30px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h5',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H5', 'hood-theme' ),
                'output'    => array( 'h5' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '14px',
                    'line-height' => '25px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h6',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H6', 'hood-theme' ),
                'output'    => array( 'h6' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( '', 'hood-theme' ),
                'default'     => array(
                    'color'       => '#727070',
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '12px',
                    'line-height' => '20px'
                ),
            ),
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social Media', 'hood-theme' ),
        'id'         => 'opt-text-social-options',
        'icon'   => 'el el-globe',
        'fields'     => array(
             array(
                'id'       => 'soc_face',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Facebook', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
             array(
                'id'       => 'soc_twitter',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Twitter', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
             array(
                'id'       => 'soc_goplus',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Google Plus', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
             array(
                'id'       => 'soc_instagram',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Instagram', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
             array(
                'id'       => 'soc_linkedin',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'LinkedIn', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
              array(
                'id'       => 'soc_pinterest',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Pinterest', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),
              array(
                'id'       => 'soc_youtube',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Youtube', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => '',
            ),

        ),

    ) );

    // Menu
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom Css & Codes', 'hood-theme' ),
        'id'         => 'hood-custom-css',
        'icon'   => 'el el-pencil',
        'fields'     => array(
        	array(
                'id'       => 'opt-ace-editor-css',
                'type'     => 'ace_editor',
                'title'    => __( 'CSS Code', 'hood-theme' ),
                'subtitle' => __( 'Paste your CSS code here.', 'hood-theme' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
            ),
			array(
				'id'       => 'hood-before-head',
				'type'     => 'ace_editor',
				'title'    => __( 'Code to be placed after &lt;head&gt; tag', 'hood-theme' ),
				'subtitle' => __( 'Code here will be added just before the closing of &lt;head&gt; tag.', 'hood-theme' ),
				'mode'     => 'text',
				'theme'    => 'eclipse',
				'options'  => array( 'minLines' => 15, 'maxLines' => 80 )
			),
			array(
				'id'       => 'hood-before-body',
				'type'     => 'ace_editor',
				'title'    => __( 'Code to be placed before closing body tag. i.e. &lt;/body&gt; tag', 'hood-theme' ),
				'subtitle' => __( 'Code here will be added just before the closing of &lt;body&gt; tag.', 'hood-theme' ),
				'mode'     => 'text',
				'theme'    => 'eclipse',
				'options'  => array( 'minLines' => 15, 'maxLines' => 80 )
			),
       ),

    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( '404 Error Page', 'hood-theme' ),
        'id'         => 'error-page',
        'icon'   => 'el el-glasses',
        'fields'     => array(
            array(
                'id'       => 'search-on',
                'type'     => 'switch',
                'title'    => esc_html__( 'Search Box', 'hood-theme' ),
                'subtitle' => esc_html__( 'Show Search Box, On or Off', 'hood-theme' ),
                'default'  => true,
            ),
             array(
                'id'       => 'not-found-text',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Not Found Text', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => 'NOT FOUND',
            ),
             array(
                'id'       => 'not-found-content',
                'type'     => 'editor',
                'title'    => esc_html__( 'Not Found Text', 'hood-theme' ),
                'subtitle' => esc_html__( '', 'hood-theme' ),
                'default'  => 'It looks like nothing was found at this location. Maybe try one of the links below or a search?',
            ),


        ),

    ) );





    /*
     * <--- END SECTIONS
     */

 add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);
 function compiler_action($options, $css, $changed_values) {
    global $wp_filesystem;

    $filename = get_template_directory() . '/assets/css/hood-custom-style.css';


    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }

    if( $wp_filesystem ) {
        $wp_filesystem->put_contents(
            $filename,
            $css,
            FS_CHMOD_FILE // predefined mode settings for WP files
        );
    }
}




