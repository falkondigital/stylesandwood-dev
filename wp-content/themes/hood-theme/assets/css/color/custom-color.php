<?php
 $absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
 $wp_load = $absolute_path[0] . 'wp-load.php';
 require_once($wp_load);

  /*
  Do whatever here...
  */

  header('Content-type: text/css');
  header('Cache-control: must-revalidate');

global $hood_options;
$hood_skin = $hood_options['general-custom-web-color'];


?>
a {
    color: <?php echo esc_attr($hood_skin); ?>
}
.text-primary {
    color: <?php echo esc_attr($hood_skin); ?>
}
.bg-primary {
    background-color: <?php echo esc_attr($hood_skin); ?>
}
.btn-primary {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.btn-primary:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.badge {
    color: <?php echo esc_attr($hood_skin); ?>;
}
.btn-link {
    color: <?php echo esc_attr($hood_skin); ?>;
}
.dropdown-menu>.active>a,
.dropdown-menu>.active>a:focus,
.dropdown-menu>.active>a:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.nav .open>a,
.nav .open>a:focus,
.nav .open>a:hover {
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.nav-pills>li.active>a,
.nav-pills>li.active>a:focus,
.nav-pills>li.active>a:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>
}
.pagination>li>a,
.pagination>li>span {
    color: <?php echo esc_attr($hood_skin); ?>;
}
.pagination>.active>a,
.pagination>.active>a:focus,
.pagination>.active>a:hover,
.pagination>.active>span,
.pagination>.active>span:focus,
.pagination>.active>span:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.label-primary {
    background-color: <?php echo esc_attr($hood_skin); ?>
}
.list-group-item.active>.badge,
.nav-pills>.active>a>.badge {
    color: <?php echo esc_attr($hood_skin); ?>;
}
a.thumbnail.active,
a.thumbnail:focus,
a.thumbnail:hover {
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.progress-bar {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.list-group-item.active,
.list-group-item.active:focus,
.list-group-item.active:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.panel-primary {
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.panel-primary>.panel-heading {
    background-color: <?php echo esc_attr($hood_skin); ?>;
    border-color: <?php echo esc_attr($hood_skin); ?>
}
.panel-primary>.panel-heading+.panel-collapse>.panel-body {
    border-top-color: <?php echo esc_attr($hood_skin); ?>
}
.panel-primary>.panel-heading .badge {
    color: <?php echo esc_attr($hood_skin); ?>;
}
.panel-primary>.panel-footer+.panel-collapse>.panel-body {
    border-bottom-color: <?php echo esc_attr($hood_skin); ?>
}

.c-post-section .c-post-gallery .owl-buttons > div:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
::-moz-selection {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}

::selection {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.mob-menu li a:hover,
.mob-menu .current-menu-item > a,
.mob-menu .current-menu-parent > a,
.mob-menu .current-menu-ancestor > a,
.mob-menu ul li > a:hover > .di,
.mob-menu .current-menu-item > a > .di,
.mob-menu .current-menu-parent > a > .di,
.mob-menu .current-menu-ancestor > a > .di,
.mob-header-content .header-top-menu li a:hover,
.mob-header-content .header-top-menu .current-menu-item > a,
.mob-header-content .header-top-menu .current-menu-parent > a,
.mob-header-content .header-top-menu .current-menu-ancestor > a,
.mob-header-content .header-top-menu ul li > a:hover > .di,
.mob-header-content .header-top-menu .current-menu-item > a > .di,
.mob-header-content .header-top-menu .current-menu-parent > a > .di,
.mob-header-content .header-top-menu .current-menu-ancestor > a > .di, {
    color: <?php echo esc_attr($hood_skin); ?> !important;
}

.sidebar a:not(.button):not(.ui-slider-handle) {
    color: <?php echo esc_attr($hood_skin); ?>;
}

.version1 .header-top {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.count-icon,
.event-icon  {
    color: <?php echo esc_attr($hood_skin); ?>;
}

.version1 .desk-cart-toggle span {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version1 .woo-cart .widget_shopping_cart p.buttons > a {
  
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version1.effect-underline .desk-menu > ul > li > a:after,
.version1.effect-overline .desk-menu > ul > li > a:after,
.version1.effect-fill .desk-menu > ul > li:hover > a,
.version1.effect-fill .desk-menu > ul > li.one-page-active > a,
.version1.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version1.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version1.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.version1.effect-outline .desk-menu > ul > li:hover > a,
.version1.effect-outline .desk-menu > ul > li.one-page-active > a,
.version1.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version1.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version1.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}

.version1.fixed-true.active .desk-cart-toggle span {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version1.fixed-true.active.effect-outline .desk-menu > ul > li:hover > a,
.version1.fixed-true.active.effect-outline .desk-menu > ul > li.one-page-active > a,
.version1.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version1.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version1.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}
.version1.fixed-true.active.effect-underline .desk-menu > ul > li > a:after,
.version1.fixed-true.active.effect-overline .desk-menu > ul > li > a:after,
.version1.fixed-true.active.effect-fill .desk-menu > ul > li:hover > a,
.version1.fixed-true.active.effect-fill .desk-menu > ul > li.one-page-active > a,
.version1.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version1.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version1.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version2 .desk-cart-toggle span {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version2 .woo-cart .widget_shopping_cart p.buttons > a {
 
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version2.effect-underline .desk-menu > ul > li > a:after,
.version2.effect-overline .desk-menu > ul > li > a:after,
.version2.effect-fill .desk-menu > ul > li:hover > a,
.version2.effect-fill .desk-menu > ul > li.one-page-active > a,
.version2.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version2.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version2.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
 
}
.version2.effect-outline .desk-menu > ul > li:hover > a,
.version2.effect-outline .desk-menu > ul > li.one-page-active > a,
.version2.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version2.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version2.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}

.version2.fixed-true.active.effect-outline .desk-menu > ul > li:hover > a,
.version2.fixed-true.active.effect-outline .desk-menu > ul > li.one-page-active > a,
.version2.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version2.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version2.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}
.version2.fixed-true.active.effect-underline .desk-menu > ul > li > a:after,
.version2.fixed-true.active.effect-overline .desk-menu > ul > li > a:after,
.version2.fixed-true.active.effect-fill .desk-menu > ul > li:hover > a,
.version2.fixed-true.active.effect-fill .desk-menu > ul > li.one-page-active > a,
.version2.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version2.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version2.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version3 .desk-cart-toggle span {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version3 .woo-cart .widget_shopping_cart p.buttons > a {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version3.effect-underline .desk-menu > ul > li > a:after,
.version3.effect-overline .desk-menu > ul > li > a:after,
.version3.effect-fill .desk-menu > ul > li:hover > a,
.version3.effect-fill .desk-menu > ul > li.one-page-active > a,
.version3.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version3.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version3.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>
}
.version3.effect-outline .desk-menu > ul > li:hover > a,
.version3.effect-outline .desk-menu > ul > li.one-page-active > a,
.version3.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version3.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version3.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}
.version3.fixed-true.active .desk-cart-toggle span {

    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.version3.fixed-true.active.effect-outline .desk-menu > ul > li:hover > a,
.version3.fixed-true.active.effect-outline .desk-menu > ul > li.one-page-active > a,
.version3.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-item > a,
.version3.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-parent > a,
.version3.fixed-true.active.effect-outline .desk-menu > ul > li.current-menu-ancestor > a {
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}
.version3.fixed-true.active.effect-underline .desk-menu > ul > li > a:after,
.version3.fixed-true.active.effect-overline .desk-menu > ul > li > a:after,
.version3.fixed-true.active.effect-fill .desk-menu > ul > li:hover > a,
.version3.fixed-true.active.effect-fill .desk-menu > ul > li.one-page-active > a,
.version3.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-item > a,
.version3.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.version3.fixed-true.active.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.one-page-bullets a[href*="#"]:hover,
.one-page-bullets .one-page-active a[href*="#"] {
    box-shadow: inset 0 0 0 10px <?php echo esc_attr($hood_skin); ?>;
}
.desk-menu > ul > li > a > .txt .label:before {
    border-color: <?php echo esc_attr($hood_skin); ?> transparent transparent transparent;
}

.header3 .version1.effect-underline .desk-menu > ul > li > a:after,
.header3 .version1.effect-overline .desk-menu > ul > li > a:after,
.header3 .version1.effect-fill .desk-menu > ul > li:hover > a,
.header3 .version1.effect-fill .desk-menu > ul > li.one-page-active > a,
.header3 .version1.effect-fill .desk-menu > ul > li.current-menu-item > a,
.header3 .version1.effect-fill .desk-menu > ul > li.current-menu-parent > a,
.header3 .version1.effect-fill .desk-menu > ul > li.current-menu-ancestor > a {
    background-color: <?php echo esc_attr($hood_skin); ?>;
    box-shadow: inset 0 0 0 2px <?php echo esc_attr($hood_skin); ?>;
}
.header3 .desk-menu > ul > li:hover > a,
.header3 .desk-menu > ul > li.one-page-active > a,
.header3 .desk-menu > ul > li.current-menu-item > a,
.header3 .desk-menu > ul > li.current-menu-parent > a,
.header3 .desk-menu > ul > li.current-menu-ancestor > a {
    color: <?php echo esc_attr($hood_skin); ?>;
}
.header4 .version1 .header-body {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}
.header7 .version1 .header-top {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.header9 .version1 .header-body {
    background-color: <?php echo esc_attr($hood_skin); ?>;
}

.br-top3 {
    border-top: 3px solid <?php echo esc_attr($hood_skin); ?>;
}

.right-pie {
    background: <?php echo esc_attr($hood_skin); ?>;

}

.pull-right {
    background: <?php echo esc_attr($hood_skin); ?>;
}

.wp-pagenavi span.current {

    background: <?php echo esc_attr($hood_skin); ?>;
}

.widget-area a:hover {
    color: <?php echo esc_attr($hood_skin); ?>;
}

.current {

    background: <?php echo esc_attr($hood_skin); ?>;
}

.page-link a {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}


p.demo_store {

    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce a.remove {

    color: <?php echo esc_attr($hood_skin); ?>!important;

}

.woocommerce span.onsale {

    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce #respond input#submit.alt.disabled,
.woocommerce #respond input#submit.alt.disabled:hover,
.woocommerce #respond input#submit.alt:disabled,
.woocommerce #respond input#submit.alt:disabled:hover,
.woocommerce #respond input#submit.alt:disabled[disabled],
.woocommerce #respond input#submit.alt:disabled[disabled]:hover,
.woocommerce a.button.alt.disabled,
.woocommerce a.button.alt.disabled:hover,
.woocommerce a.button.alt:disabled,
.woocommerce a.button.alt:disabled:hover,
.woocommerce a.button.alt:disabled[disabled],
.woocommerce a.button.alt:disabled[disabled]:hover,
.woocommerce button.button.alt.disabled,
.woocommerce button.button.alt.disabled:hover,
.woocommerce button.button.alt:disabled,
.woocommerce button.button.alt:disabled:hover,
.woocommerce button.button.alt:disabled[disabled],
.woocommerce button.button.alt:disabled[disabled]:hover,
.woocommerce input.button.alt.disabled,
.woocommerce input.button.alt.disabled:hover,
.woocommerce input.button.alt:disabled,
.woocommerce input.button.alt:disabled:hover,
.woocommerce input.button.alt:disabled[disabled],
.woocommerce input.button.alt:disabled[disabled]:hover {
    background-color: <?php echo esc_attr($hood_skin); ?>;

}


.woocommerce .star-rating {

    color: <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce .woocommerce-product-rating .star-rating {
   
    color: <?php echo esc_attr($hood_skin); ?>;
 
}

.woocommerce .widget_price_filter .ui-slider .ui-slider-handle {

    background-color: <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce .widget_price_filter .ui-slider .ui-slider-range {

    background-color: <?php echo esc_attr($hood_skin); ?>
}

.woocommerce-error,
.woocommerce-info,
.woocommerce-message {

    border-top: 3px solid <?php echo esc_attr($hood_skin); ?>;

}

.woocommerce-message {
    border-top-color: <?php echo esc_attr($hood_skin); ?>
}

.woocommerce-message:before {
 
    color: <?php echo esc_attr($hood_skin); ?>
}

.hood-text-skin {

    color: <?php echo esc_attr($hood_skin); ?>;

}