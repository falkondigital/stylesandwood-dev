<?php
/**
* The template for displaying all single posts.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Hood Theme
*/
get_header(); ?>
<?php 
global $hood_options;
?>
<!-- C-LAYOUT-BREADCRUMBS -->
<section class="c-layout-breadcrumbs-1">
  <div class="container">
    <h1 class="c-page-title">
      <?php the_title(); ?>
    </h1>
  </div>
  <!--/.container -->
</section>
<!-- END C-LAYOUT-BREADCRUMBS -->
<!-- primary -->
<div id="primary" class="content-area hood_mt35">
  <!-- main -->
  <main id="main" class="site-main">
    <div class="container">
      <article id="post-<?php the_ID(); ?>" 
               <?php post_class(); ?>>
      <div class="entry-content">
        <div class="c-content-boxes variant-two ">
          <div class="col-md-12">
            <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php endwhile; // End of the loop. ?>
          </div>
        </div>
      </div>
      </article>
    <!-- #post-## -->
    </div>
  <?php wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink=page %'); ?>
  </main>
<!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>