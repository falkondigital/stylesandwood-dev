<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hood Theme
 */

?>

<div class="container">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <!-- BEGIN C-LAYOUT-SIDEBAR -->
    <div>
      <div class="container">
        <div class="row">
          <div class="col-md-9 no-padding-left">
            <h3 class="c-content-title">
              <?php the_title(); ?>

            </h3>
            <p><?php hood_theme_posted_on(); ?></p>
             <?php $featured_images = $hood_options['post-image']; ?>
          <?php if ($featured_images == '1') { ?>
           <?php if ( has_post_thumbnail() ) : ?>
              <div class="single-post-image">
      <a href="<?php the_permalink() ?>">
                <?php the_post_thumbnail(); ?>
              </a>
              </div>
                  <?php endif; ?>
              <?php }else {} ?>

            <?php the_content(); ?>
            <div class="entry-meta">


            <?php
             // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
           comments_template();
           endif;
           ?>

          </div><!-- .entry-meta -->
          <div class="col-md-12 no-padding">

          <?php
          $tags = wp_get_post_tags( $post->ID );
          if(!empty($tags)) { ?>
         <div class="post-tags">
          <ul>
           <li class="tag-single">
              <?php echo esc_html__('Tag:', 'hood-theme'); ?>
          </li>
        <?php foreach($tags as $tag) : ?>
          <li class="post-tags-item">
            <a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo esc_attr($tag->name); ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  <?php } ?>

          </div>
          <div class="single-pagination">
<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'hood-theme' ), 'after' => '</div>' ) ); ?>
</div>
          </div>

          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>

        </div>
      </div>
    </div>

    <!-- END C-LAYOUT-SIDEBAR -->
  </article>
  <!-- #post-## -->
</div>
