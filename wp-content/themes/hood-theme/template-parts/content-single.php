<?php
/**
* Template part for displaying single posts.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Hood Theme
*/
?>
<?php global $hood_options;
// Grab the metadata from the database
$text = $hood_options['layout'];
if($text == '2'){
require get_template_directory() . '/template-parts/content-single-left-sidebar.php';
}
elseif($text == '1'){
require get_template_directory() . '/template-parts/content-single-right-sidebar.php';
}
elseif($text == '0'){ ?>
<div class="container">
  <article id="post-<?php the_ID(); ?>" 
           <?php post_class(); ?>>
  <!-- BEGIN C-LAYOUT-SIDEBAR -->
  <div>
     <?php $featured_images = $hood_options['post-image']; ?>
          <?php if ($featured_images == '1') { ?>
           <?php if ( has_post_thumbnail() ) : ?>
              <div class="single-post-image">
      <a href="<?php the_permalink() ?>">
                <?php the_post_thumbnail(); ?>
              </a>
              </div>
              <?php endif; ?>
              <?php }else {} ?>
    
    <?php the_content(); ?>
    <?php hood_theme_posted_on(); ?>
    <div class="entry-meta">
      <?php
// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;
?>
    </div>
    <!-- .entry-meta -->
    <div class="col-md-12 no-padding">
      <?php
$tags = wp_get_post_tags( $post->ID );
if(!empty($tags)) { ?>
      <div class="post-tags">
        <ul>
          <li class="tag-single">
            <?php echo esc_html__('Tag:', 'hood-theme'); ?>
          </li>
          <?php foreach($tags as $tag) : ?>
          <li class="post-tags-item">
            <a href="<?php echo get_tag_link($tag->term_id); ?>">
              <?php echo esc_attr($tag->name); ?>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- END C-LAYOUT-SIDEBAR -->
  </article>
<!-- #post-## -->
</div>
<?php }else {
require get_template_directory() . '/template-parts/content-single-right-sidebar.php';
} ?>