<?php
/**
* The sidebar containing the main widget area.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Hood Theme
*/
global $hood_options;
if ( ! is_active_sidebar( 'default_sidebar' ) ) {
return;
}
?>
<?php 
if($hood_options['hood_sidebar'] == 'None'){
$hoodside = 'default_sidebar';
}else {
$hoodside = $hood_options['hood_sidebar'];
}
?>
<div id="secondary" class="widget-area" role="complementary">
  <?php dynamic_sidebar( $hoodside ); ?>
</div>